<%@include file="includes/prefix.jsp" %>

<head>
    <%@include file="includes/head.jsp" %>
    <link href="/app/css/chat.css" rel="stylesheet">
    <link href="/app/css/pokemonMemoryCard.css" rel="stylesheet">
    <title>Hello WebSocket</title>
</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <%@include file="includes/sidebar.jsp" %>

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

    <section class="jumpers">

      <!-- Main Content -->
      <div id="content">

        <%@include file="includes/topbar.jsp" %>
        <!-- Begin Page Content -->
        <div class="container">

          <div class="row">

            <div class="col-md-8">
                <div class="card shadow">
                    <div class="card-body">

                        <div class="pokemonMemoryBoard">

                            <div class="container-fluid">

                            	<div class="row">

                                    <div class="col-sm-4 col-lg-3" >
                                        <figure>
                                            <div class="pokemonCard" id="c0"><img style="opacity:0;" src="/app/img/pokemonMemoryCard/card.png"></div>
                                        </figure>
                                    </div>

                                    <div class="col-sm-4 col-lg-3">
                                        <figure>
                                            <div class="pokemonCard" id="c1"><img style="opacity:0;" src="/app/img/pokemonMemoryCard/card.png"></div>
                                        </figure>
                                    </div>

                                    <div class="col-sm-4 col-lg-3">
                                        <figure>
                                            <div class="pokemonCard" id="c2"><img style="opacity:0;" src="/app/img/pokemonMemoryCard/card.png"></div>
                                        </figure>
                                    </div>

                                    <div class="col-sm-4 col-lg-3">
                                        <figure>
                                            <div class="pokemonCard" id="c3"><img style="opacity:0;" src="/app/img/pokemonMemoryCard/card.png"></div>
                                        </figure>
                                    </div>

                                    <div class="col-sm-4 col-lg-3">
                                        <figure>
                                            <div class="pokemonCard" id="c4"><img style="opacity:0;" src="/app/img/pokemonMemoryCard/card.png"></div>
                                        </figure>
                                    </div>

                                    <div class="col-sm-4 col-lg-3">
                                        <figure>
                                            <div class="pokemonCard" id="c5"><img style="opacity:0;" src="/app/img/pokemonMemoryCard/card.png"></div>
                                        </figure>
                                    </div>

                                    <div class="col-sm-4 col-lg-3">
                                        <figure>
                                            <div class="pokemonCard" id="c6"><img style="opacity:0;" src="/app/img/pokemonMemoryCard/card.png"></div>
                                        </figure>
                                    </div>

                                    <div class="col-sm-4 col-lg-3">
                                        <figure>
                                            <div class="pokemonCard" id="c7"><img style="opacity:0;" src="/app/img/pokemonMemoryCard/card.png"></div>
                                        </figure>
                                    </div>

                                    <div class="col-sm-4 col-lg-3">
                                        <figure>
                                            <div class="pokemonCard" id="c8"><img style="opacity:0;" src="/app/img/pokemonMemoryCard/card.png"></div>
                                        </figure>
                                     </div>

                                    <div class="col-sm-4 col-lg-3">
                                        <figure>
                                            <div class="pokemonCard" id="c9"><img style="opacity:0;" src="/app/img/pokemonMemoryCard/card.png"></div>
                                        </figure>
                                     </div>

                                    <div class="col-sm-4 col-lg-3">
                                        <figure>
                                            <div class="pokemonCard" id="c10"><img style="opacity:0;" src="/app/img/pokemonMemoryCard/card.png"></div>
                                        </figure>
                                    </div>

                                    <div class="col-sm-4 col-lg-3">
                                        <figure>
                                            <div class="pokemonCard" id="c11"><img style="opacity:0;" src="/app/img/pokemonMemoryCard/card.png"></div>
                                        </figure>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="row">

                    <div class="col-md-12">
                        <div class="card shadow">
                            <div class="card-body">
                                <center id = "gameStatus">
                                </center>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="card shadow">

                            <div class="row">

                                <div class="col-md-4">
                                    <div class="card-body">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Player</div>
                                        <div id = "username1" class="h5 mb-0 font-weight-bold text-gray-800">...</div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="card-body">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Score</div>
                                        <div id ="user1Score" class="h5 mb-0 font-weight-bold text-gray-800">0</div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="card-body">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Timeout</div>
                                        <div id = "timeout1" class="h5 mb-0 font-weight-bold text-gray-800">0</div>
                                    </div>
                                </div>

                                <div class="col">
                                    <div class="progress progress-sm mr-2">
                                        <div id = "timeoutBar1" class="progress-bar bg-info" role="progressbar" style="width: 30%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="card shadow">

                            <div class="row">

                                <div class="col-md-4">
                                    <div class="card-body">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Player</div>
                                        <div id = "username2" class="h5 mb-0 font-weight-bold text-gray-800">...</div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="card-body">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Score</div>
                                        <div id = "user2Score" class="h5 mb-0 font-weight-bold text-gray-800">0</div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="card-body">
                                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Timeout</div>
                                        <div id = "timeout2" class="h5 mb-0 font-weight-bold text-gray-800">0</div>
                                    </div>
                                </div>

                                <div class="col">
                                    <div class="progress progress-sm mr-2">
                                        <div id = "timeoutBar2" class="progress-bar bg-info" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>

            </div>

            <div class="col-md-4">
                <div class="card shadow">
                    <div class="card-body">
                        <div class="mesgs">
                            <div id="msg_history" class="msg_history">
                            </div>
                            <div class="type_msg">
                              <div class="input_msg_write">
                                <input id="textBosMsg" type="text" class="write_msg" placeholder="Type a message" />
                                <!-- <button id="send" class="msg_send_btn" type="button"><i class="fa fa-paper-plane-o" aria-hidden="false"></i></button> -->
                                <button id="send" class="msg_send_btn" type="button"></button>
                              </div>
                            </div>
                        </div>
                    </div>
                 </div>
            </div>

          </div>
        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      </section>

    <%@include file="includes/footer.jsp" %>

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <%@include file="includes/scrollTop.jsp" %>
  <%@include file="includes/script.jsp" %>

  <script>var userId =<c:out value="${userId}"/></script>
  <script>var roomId =<c:out value="${roomId}"/></script>
  <script src="/app/js/websocket2.js"></script>
  <script src="/app/js/websocketPokemonMemoryCard.js"></script>
  <script src="/webjars/sockjs-client/1.0.2/sockjs.min.js"></script>
  <script src="/webjars/stomp-websocket/2.3.3/stomp.min.js"></script>

</body>

</html>
