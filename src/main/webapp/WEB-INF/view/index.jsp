<%@include file="includes/prefix.jsp" %>

<head>
    <%@include file="includes/head.jsp" %>
    <title>SB Admin 2 - Dashboard</title>
</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <%@include file="includes/sidebar.jsp" %>

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <%@include file="includes/topbar.jsp" %>
        <!-- Begin Page Content -->
        <div class="container-fluid">


        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

    <%@include file="includes/footer.jsp" %>

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <%@include file="includes/scrollTop.jsp" %>
  <%@include file="includes/script.jsp" %>

</body>

</html>
