  <!-- Bootstrap core JavaScript 1 -->
  <script src="/webjars/jquery/3.4.1/jquery.min.js"></script>
  <script src="/webjars/bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript -->
  <script src="/webjars/jquery-easing/1.4.1/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages 1 -->
  <script src="/sb-admin-2/js/sb-admin-2.js"></script>

  <!-- Page level plugins 1 -->
  <script src="/sb-admin-2/js/Chart.min.js"></script>

  <!-- Page level custom scripts 1 -->
  <script src="/sb-admin-2/js/chart-area-demo.js"></script>
  <script src="/sb-admin-2/js/chart-pie-demo.js"></script>

