<%@include file="includes/prefix.jsp" %>

<head>
    <%@include file="includes/head.jsp" %>
    <title>SB Admin 2 - Dashboard</title>
</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <%@include file="includes/sidebar.jsp" %>

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <%@include file="includes/topbar.jsp" %>
        <!-- Begin Page Content -->
        <div class="container-fluid">

          <div class="row">

            <div class="col-lg-6">
              <div class="card shadow mb-1">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">Available games</h6>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                      <thead>
                        <tr>
                          <th>Id</th>
                          <th>Game</th>
                          <th>Player</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        <c:forEach var="r" items="${freeRooms}">
                          <tr>
                            <td>${r.id}</td>
                            <td>${r.game.name}</td>
                            <th>${r.user1.username}</th>
                            <th>
                              <a href="/room/${r.id}/join" class="btn btn-success btn-icon-split">
                                <span class="text">Join!</span>
                              </a>
                            </th>
                          </tr>
                        </c:forEach>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-lg-6">
              <div class="card shadow mb-1">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">My active games</h6>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                      <thead>
                        <tr>
                          <th>Id</th>
                          <th>Game</th>
                          <th>Player</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        <c:forEach var="r" items="${userRooms}">
                          <tr>
                            <td>${r.id}</td>
                            <td>${r.game.name}</td>
                            <th>${r.user1.username}</th>
                            <th>
                              <a href="/room/${r.id}/show" class="btn btn-primary btn-icon-split">
                                <span class="text">Back!</span>
                              </a>
                            </th>
                          </tr>
                        </c:forEach>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>

          </div>

          <div class="row">


            <div class="col-lg-6">
              <div class="card shadow mb-1">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">Create new one</h6>
                </div>
                <div class="card-body">

                  <form:form action="/room/add" modelAttribute="createGameDTO" method="post">
                    <div class="form-group">
                      <form:select class="form-control form-control-user" path="id" id="exampleFormControlSelect1">
                        <c:forEach var="g" items="${gamesList}">
                          <option value="${g.id}">${g.name}</option>
                        </c:forEach>
                      </form:select>
                    </div>
                    <input type="submit" value="Create" class="btn btn-primary btn-user btn-block" />
                  </form:form>

                </div>
              </div>
            </div>






          </div>




        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

    <%@include file="includes/footer.jsp" %>

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <%@include file="includes/scrollTop.jsp" %>
  <%@include file="includes/script.jsp" %>


  <script src="/datatables/jquery.dataTables.min.js"></script>
  <script src="/datatables/dataTables.bootstrap4.min.js"></script>

  <script src="sb-admin-2/js/datatables-demo.js"></script>


</body>

</html>
