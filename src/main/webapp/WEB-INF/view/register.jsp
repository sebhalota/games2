<%@include file="includes/prefix.jsp" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<head>
  <%@include file="includes/head.jsp" %>
  <title>SB Admin 2 - Register</title>
</head>

<body class="bg-gradient-primary">

  <div class="container">

    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <!-- <div class="col-lg-5 d-none d-lg-block bg-register-image"></div> -->
          <div class="col-lg-5 d-none d-lg-block"></div>
          <div class="col-lg-7">
            <div class="p-5">
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">Create an Account!</h1>
              </div>

                <form action="/register" method="post" class="user">

                  <div class="form-group row">

                    <div class="col-sm-6 mb-3 mb-sm-0">
                      <spring:bind path="usernameDTO.username">
                        <c:set var="valid" value = "" />
                        <c:if test="${pageContext.request.method=='POST'}">
                          <c:set var="valid" value = "is-valid" />
                          <c:if test="${not empty status.errorMessage}">
                              <c:set var="valid" value = "is-invalid" />
                          </c:if>
                        </c:if>
                        <input type="text" name="${status.expression}" value="${status.value}" class="form-control form-control-user ${valid}" placeholder="Username">
                        <small class="text-danger">${status.errorMessage}</small>
                      </spring:bind>
                    </div>

                    <div class="col-sm-6">
                      <spring:bind path="emailDTO.email">
                        <c:set var="valid" value = "" />
                        <c:if test="${pageContext.request.method=='POST'}">
                          <c:set var="valid" value = "is-valid" />
                          <c:if test="${not empty status.errorMessage}">
                              <c:set var="valid" value = "is-invalid" />
                          </c:if>
                        </c:if>
                        <input type="text" name="${status.expression}" value="${status.value}" class="form-control form-control-user ${valid}" placeholder="Email address">
                        <small class="text-danger">${status.errorMessage}</small>
                      </spring:bind>
                    </div>
                  </div>

                  <div class="form-group row">

                    <spring:bind path="passwordDTO">
                      <c:set var="valid" value = "" />
                      <c:if test="${pageContext.request.method=='POST'}">
                      <c:set var="valid" value = "is-valid" />
                      <c:if test="${not empty status.errorMessage}">
                         <c:set var="valid" value = "is-invalid" />
                      </c:if>
                    </c:if>
                    </spring:bind>

                    <div class="col-sm-6 mb-3 mb-sm-0">
                      <spring:bind path="passwordDTO.password">
                        <input type="password" name="${status.expression}" value="${status.value}" class="form-control form-control-user ${valid}" placeholder="Password">
                      </spring:bind>
                    </div>

                    <div class="col-sm-6">
                      <spring:bind path="passwordDTO.matchingPassword">
                        <input type="password" name="${status.expression}" value="${status.value}" class="form-control form-control-user ${valid}" placeholder="Password">
                      </spring:bind>
                    </div>

                    <div class="col-sm-12 mb-3 mb-sm-0">
                      <spring:bind path="passwordDTO">
                        <small class="text-danger">${status.errorMessage}</small>
                      </spring:bind>
                    </div>

                  </div>

                  <input type="submit" value="Register Account" class="btn btn-primary btn-user btn-block" />

                </form>
              <hr>

              <div class="text-center">
                <a class="small" href="forgot-password">Forgot Password?</a>
              </div>
              <div class="text-center">
                <a class="small" href="login">Already have an account? Login!</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>

  <%@include file="includes/script.jsp" %>

</body>

</html>
