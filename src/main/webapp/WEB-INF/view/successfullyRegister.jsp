<%@include file="includes/prefix.jsp" %>
<head>
    <%@include file="includes/head.jsp" %>
</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <div>
                    <!-- Error Text -->
                        <div class="text-center">
                            <div class="error mx-auto">
                                <i class="fas fa-smile"></i>
                            </div>
                            <p class="text-gray-500 mb-0">Your account has been successfully created!</p>
                            <a href="/login">Go to login page &rarr;</a>
                        </div>
                    </div>
                </div>

                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

  <%@include file="includes/scrollTop.jsp" %>
  <%@include file="includes/script.jsp" %>

</body>

</html>
