package com.games.games.timeout;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
@Scope("singleton")
public class TimeoutTimerPool {

//    private Map<Long, TimeoutTimer2> map = Collections.synchronizedMap(new HashMap<>());
    private Map<Long, TimeoutTimer> map = new HashMap<>();

    public TimeoutTimer getTimeoutTimer(long id) {
        if(!map.containsKey(id)) {
            System.out.println("Nie zawiera!");
            TimeoutTimer timeoutTimer2 = new TimeoutTimer(id);
            map.put(id,timeoutTimer2);
        } else {
            System.out.println("Zawiera!");
        }

        return map.get(id);

    }

}
