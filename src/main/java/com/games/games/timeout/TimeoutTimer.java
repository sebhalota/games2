package com.games.games.timeout;

import com.games.games.timeout.observer.Observable;
import com.games.games.timeout.observer.Observer;

import java.util.Timer;
import java.util.TimerTask;

public class TimeoutTimer implements Observable {

    private Observer observer;

    private Timer timer;
    private int seconds;
    private long id;
    private boolean started;


    TimeoutTimer(long id) {
        this.id = id;
        this.timer = new Timer();
    }

    @Override
    public void notifyObservers() {
        observer.update(id);
    }

    public void start(int seconds, Observer observer) {

        this.seconds = seconds;
        this.observer = observer;

        if (started) {
            System.out.println("Juz byl wystarwowany");
        } else {
            System.out.println("Nie byl wystartowany");
            start();
        }

    }

    private void start() {
        this.timer.schedule(new TimerTick(), 1000);
        this.started = true;
    }

    private void stop() {
        System.out.println("TimeoutTimer2 stop");
        started = false;
    }

    private class TimerTick extends TimerTask {
        public void run() {

            if (seconds > 0) {
                seconds--;
                System.out.println("tick" + id + " seconds left: " + seconds);
                start();
            } else {
                stop();
                notifyObservers();
            }
        }

    }

    public int getSecondsLeft() {
        return seconds;
    }


}
