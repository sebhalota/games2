package com.games.games.timeout.observer;

public interface Observer {
    void update(long id);
}
