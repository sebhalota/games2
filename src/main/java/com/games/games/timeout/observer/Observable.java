package com.games.games.timeout.observer;

public interface Observable {
    void notifyObservers();
}
