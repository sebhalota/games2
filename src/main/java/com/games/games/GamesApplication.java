package com.games.games;

import com.games.games.testingData.TestingData;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class GamesApplication extends SpringBootServletInitializer {


	public static void main(String[] args)
	{
		ConfigurableApplicationContext context = SpringApplication.run(GamesApplication.class, args);

		context.getBean(TestingData.class).fillDatabaseWithTestData();
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(GamesApplication.class);
	}

}
