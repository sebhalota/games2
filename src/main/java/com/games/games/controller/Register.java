package com.games.games.controller;

import com.games.games.config.WebSecurityConfig;
import com.games.games.dto.registerDTO.EmailDTO;
import com.games.games.dto.registerDTO.PasswordDTO;
import com.games.games.dto.registerDTO.UsernameDTO;
import com.games.games.model.entities.User;
import com.games.games.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

@Controller
public class Register {

    @Autowired
    private UserService userService;

    @RequestMapping(value="/register", method = RequestMethod.GET)
    public String registerGET(ModelMap modelMap) {

        UsernameDTO usernameDTO = new UsernameDTO();
        EmailDTO emailDTO = new EmailDTO();
        PasswordDTO passwordDTO = new PasswordDTO();

        modelMap.addAttribute("usernameDTO",usernameDTO);
        modelMap.addAttribute("emailDTO",emailDTO);
        modelMap.addAttribute("passwordDTO",passwordDTO);

        return "register";

    }

    @RequestMapping(value="/register", method = RequestMethod.POST)
    public String registerPOST(@ModelAttribute("usernameDTO") @Valid UsernameDTO usernameDTO, BindingResult resultUsernameDTO,
                               @ModelAttribute("emailDTO") @Valid EmailDTO emailDTO, BindingResult resultEmailDTO,
                               @ModelAttribute("passwordDTO") @Valid PasswordDTO passwordDTO, BindingResult resultPasswordDTO
                               ) {

        if(!resultUsernameDTO.hasErrors()) {
            if(!resultEmailDTO.hasErrors()) {
                if(!resultPasswordDTO.hasErrors()) {
                    User user = new User();
                    user.setUsername(usernameDTO.getUsername());
                    user.setEmail(emailDTO.getEmail());
                    user.setPassword(WebSecurityConfig.encoder().encode(passwordDTO.getPassword()));
                    user.setEnable(true);

                    User savedUser = userService.save(user);

                    if(savedUser.equals(user)) {
                        return "successfullyRegister";
                    }
                    else {
                        throw new RuntimeException();
                    }

                }
            }
        }

        return "register";
    }

}
