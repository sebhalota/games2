package com.games.games.controller;

import com.games.games.PokemonMemoryCard.CardStatus;
import com.games.games.timeout.observer.Observer;
import com.games.games.PokemonMemoryCard.PokemonMemoryCardClientData;
import com.games.games.PokemonMemoryCard.PokemonMemoryCardGameData;
import com.games.games.PokemonMemoryCard.RevealCardRequest;
import com.games.games.model.entities.Room;
import com.games.games.service.GameService;
import com.games.games.service.RoomService;
import com.games.games.timeout.TimeoutTimer;
import com.games.games.timeout.TimeoutTimerPool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

import java.security.Principal;

import static com.games.games.PokemonMemoryCard.CardStatus.CardStatusEnum.*;
import static com.games.games.model.entities.Room.GameStatus.*;

//TODO zobaczyc observePattern

@Controller
public class PokemonMemoryCard {

    @Autowired
    private RoomService roomService;

    @Autowired
    private GameService gameService;

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    @Autowired
    private TimeoutTimerPool timeoutTimerPool;


    @MessageMapping("/pokemonMemoryCard/{id}/reveal")
    public void reveal(@Payload RevealCardRequest revealCardRequest, @DestinationVariable("id") long id, Principal principal) {

        Room room = roomService.findById(id);
        PokemonMemoryCardGameData pokemonMemoryCardGameData = new PokemonMemoryCardGameData(room.getGameData());
        TimeoutTimer timeoutTimer = timeoutTimerPool.getTimeoutTimer(id);

        boolean correctUser = false;
        int reqCard = 0;
        boolean availableCard = false;
        boolean endOfTime = false;
        boolean sendMessage = false;
        int timeout1 = 0;
        int timeout2 = 0;
        boolean updateTimeout = false;

        if (revealCardRequest != null) {
            correctUser = isCorrectUser(principal, room);
            reqCard = revealCardRequest.getCardNr();
            availableCard = isAvailableCard(pokemonMemoryCardGameData, reqCard);
        } else {
            endOfTime = true;
        }

        switch (room.getGameStatus()) {
            case USER1_FIRST_CARD: {

                if (availableCard && correctUser) {
                    pokemonMemoryCardGameData.getCardStatusList().get(reqCard).setCardStatusEnum(REVEAL);
                    room.setGameStatus(USER1_SECOND_CARD);
                } else if (endOfTime) {
                    room.setGameStatus(USER2_FIRST_CARD);
                    timeout2 = 10;
                    timeoutTimer.start(timeout2, new ObserverImpl());
                    updateTimeout = true;
                }

                sendMessage = true;
                break;
            }
            case USER1_SECOND_CARD: {

                if (availableCard && correctUser) {

                    int firstCardIndex = getFistCardIndex(pokemonMemoryCardGameData);

                    if (pokemonMemoryCardGameData.getCardStatusList().get(firstCardIndex).getImgNr() == pokemonMemoryCardGameData.getCardStatusList().get(reqCard).getImgNr()) {
                        pokemonMemoryCardGameData.getCardStatusList().get(firstCardIndex).setCardStatusEnum(REVEAL_AND_REMOVE);
                        pokemonMemoryCardGameData.getCardStatusList().get(reqCard).setCardStatusEnum(REVEAL_AND_REMOVE);
                        pokemonMemoryCardGameData.setUser1Score(pokemonMemoryCardGameData.getUser1Score() + 1);
                    } else {
                        pokemonMemoryCardGameData.getCardStatusList().get(firstCardIndex).setCardStatusEnum(REVEAL_AND_COVER);
                        pokemonMemoryCardGameData.getCardStatusList().get(reqCard).setCardStatusEnum(REVEAL_AND_COVER);
                    }

                    room.setGameStatus(USER2_FIRST_CARD);

                    timeout2 = 10;
                    timeoutTimer.start(timeout2, new ObserverImpl());
                    updateTimeout = true;

                } else if (endOfTime) {
                    room.setGameStatus(USER2_FIRST_CARD);
                    timeout2 = 10;
                    timeoutTimer.start(timeout2, new ObserverImpl());
                    updateTimeout = true;

                    for (CardStatus cardStatus : pokemonMemoryCardGameData.getCardStatusList()) {
                        if (cardStatus.getCardStatusEnum().equals(REVEAL)) {
                            cardStatus.setCardStatusEnum(COVERED);
                        }
                    }

                }

                sendMessage = true;
                break;
            }

            case USER2_FIRST_CARD: {

                if (availableCard && correctUser) {
                    pokemonMemoryCardGameData.getCardStatusList().get(reqCard).setCardStatusEnum(REVEAL);
                    room.setGameStatus(USER2_SECOND_CARD);
                } else if (endOfTime) {
                    room.setGameStatus(USER1_FIRST_CARD);
                    timeout1 = 10;
                    timeoutTimer.start(timeout1, new ObserverImpl());
                    updateTimeout = true;
                }

                sendMessage = true;
                break;

            }

            case USER2_SECOND_CARD: {

                if (availableCard && correctUser) {
                    int firstCardIndex = getFistCardIndex(pokemonMemoryCardGameData);

                    if (pokemonMemoryCardGameData.getCardStatusList().get(firstCardIndex).getImgNr() == pokemonMemoryCardGameData.getCardStatusList().get(reqCard).getImgNr()) {
                        pokemonMemoryCardGameData.getCardStatusList().get(firstCardIndex).setCardStatusEnum(REVEAL_AND_REMOVE);
                        pokemonMemoryCardGameData.getCardStatusList().get(reqCard).setCardStatusEnum(REVEAL_AND_REMOVE);
                        pokemonMemoryCardGameData.setUser2Score(pokemonMemoryCardGameData.getUser2Score() + 1);
                    } else {
                        pokemonMemoryCardGameData.getCardStatusList().get(firstCardIndex).setCardStatusEnum(REVEAL_AND_COVER);
                        pokemonMemoryCardGameData.getCardStatusList().get(reqCard).setCardStatusEnum(REVEAL_AND_COVER);
                    }

                    room.setGameStatus(USER1_FIRST_CARD);

                    timeout1 = 10;
                    timeoutTimer.start(timeout1, new ObserverImpl());
                    updateTimeout = true;

                } else if (endOfTime) {
                    room.setGameStatus(USER1_FIRST_CARD);
                    timeout1 = 10;
                    timeoutTimer.start(timeout1, new ObserverImpl());
                    updateTimeout = true;

                    for (CardStatus cardStatus : pokemonMemoryCardGameData.getCardStatusList()) {
                        if (cardStatus.getCardStatusEnum().equals(REVEAL)) {
                            cardStatus.setCardStatusEnum(COVERED);
                        }
                    }

                }
                sendMessage = true;
                break;
            }

        }


        if (gameIsOver(pokemonMemoryCardGameData)) {
            room.setGameStatus(chooseAWinner(pokemonMemoryCardGameData));
        }

        // TODO naprawic to zeby hibernate nie wykonywal selectow. Dzieje sie tak bo hibernate nie ma instancji.
        // TODO a jak chcialem wprost wszystko zapisywac to nie umiem zapisac listy...
        if (sendMessage) {
            room.setGameData(pokemonMemoryCardGameData.generateDataForDatabase());
            roomService.saveRoom(room);

            sendDataToClient(pokemonMemoryCardGameData, room, id, timeout1, timeout2,updateTimeout);
        }

    }

    @MessageMapping("/pokemonMemoryCard/{id}/refresh")
    public void refresh(@DestinationVariable("id") long id, Principal principal) {
        Room room = roomService.findById(id);

        //TODO
//        if (room.getUser1().getUsername().equals(principal.getName()) || room.getUser2().getUsername().equals(principal.getName())) {
        PokemonMemoryCardGameData pokemonMemoryCardGameData = new PokemonMemoryCardGameData(room.getGameData());
        sendDataToClient(pokemonMemoryCardGameData, room, id, 0, 0,false);
//        }


    }

    private boolean isCorrectUser(Principal principal, Room room) {

        if (principal == null || room == null) return false;

        if (room.getUser1().getUsername().equals(principal.getName())) {
            if ((room.getGameStatus().equals(USER1_FIRST_CARD)) || (room.getGameStatus().equals(USER1_SECOND_CARD)))
                return true;
        } else if ((room.getUser2().getUsername().equals(principal.getName()))) {
            if ((room.getGameStatus().equals(Room.GameStatus.USER2_FIRST_CARD)) || (room.getGameStatus().equals(Room.GameStatus.USER2_SECOND_CARD)))
                return true;
        }

        return false;
    }

    private boolean isAvailableCard(PokemonMemoryCardGameData pokemonMemoryCardGameData, int reqCard) {
        return pokemonMemoryCardGameData.getCardStatusList().get(reqCard).getCardStatusEnum().equals(COVERED);
    }

    private boolean gameIsOver(PokemonMemoryCardGameData pokemonMemoryCardGameData) {

        boolean gameEnd = true;
        for (int i = 0; i < pokemonMemoryCardGameData.getCardStatusList().size(); i++) {
            System.out.println(pokemonMemoryCardGameData.toString());
            if (!pokemonMemoryCardGameData.getCardStatusList().get(i).getCardStatusEnum().equals(REMOVED) && !pokemonMemoryCardGameData.getCardStatusList().get(i).getCardStatusEnum().equals(REVEAL_AND_REMOVE)) {
                gameEnd = false;
                break;
            }
        }

        return gameEnd;
    }

    private Room.GameStatus chooseAWinner(PokemonMemoryCardGameData pokemonMemoryCardGameData) {

        if (pokemonMemoryCardGameData.getUser1Score() > pokemonMemoryCardGameData.getUser2Score()) {
            return USER1_WON;
        } else if (pokemonMemoryCardGameData.getUser1Score() < pokemonMemoryCardGameData.getUser2Score()) {
            return USER2_WON;
        }

        return DRAW;
    }

    private int getFistCardIndex(PokemonMemoryCardGameData pokemonMemoryCardGameData) {
        int firstCardIndex = 0;

        for (int i = 0; i < pokemonMemoryCardGameData.getCardStatusList().size(); i++) {
            if (pokemonMemoryCardGameData.getCardStatusList().get(i).getCardStatusEnum().equals(REVEAL)) {
                firstCardIndex = i;
                break;
            }
        }

        return firstCardIndex;
    }

    private void sendDataToClient(PokemonMemoryCardGameData pokemonMemoryCardGameData, Room room, long id, int timeout1, int timeout2, boolean updateTimeout) {

        PokemonMemoryCardClientData data = new PokemonMemoryCardClientData(pokemonMemoryCardGameData, room);
        data.setTimeout1(timeout1);
        data.setTimeout2(timeout2);
        data.setUpdateTimeout(updateTimeout);

        simpMessagingTemplate.convertAndSend("/topic/pokemonMemoryCard/" + id + "/gameData", data);
    }

    private class ObserverImpl implements Observer {
        public void update(long id) {
            reveal(null, id, null);
        }
    }


}
