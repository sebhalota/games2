package com.games.games.controller;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;


@Controller
public class Error implements ErrorController {

    private static final String PATH = "/error";

    @RequestMapping(value = PATH)
    public String error(HttpServletRequest request, Model model) {

        Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
        int statusCode = 0;
        String message = "";

        if(status != null) {
            statusCode = Integer.parseInt(status.toString());
            message = HttpStatus.valueOf(statusCode).getReasonPhrase();
        }

        model.addAttribute("errorNumber",statusCode);
        model.addAttribute("errorMessage",message);

        return "error";
    }

    @Override
    public String getErrorPath() {
        return PATH;
    }

}
