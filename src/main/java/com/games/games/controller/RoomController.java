package com.games.games.controller;

import com.games.games.PokemonMemoryCard.PokemonMemoryCardGameData;
import com.games.games.dto.chat.IncomingMessage;
import com.games.games.dto.chat.OutgoingMessage;
import com.games.games.dto.game.CreateGameDTO;
import com.games.games.model.entities.Game;
import com.games.games.model.entities.Room;
import com.games.games.config.security.UserPrincipal;
import com.games.games.service.GameService;
import com.games.games.service.RoomService;
import com.games.games.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.*;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

@Controller
public class RoomController {

    @Autowired
    RoomService roomService;

    @Autowired
    UserService userService;

    @Autowired
    GameService gameService;

    @Autowired
    SimpMessagingTemplate simpMessagingTemplate;

    @RequestMapping("/room/all")
    public String roomAll(ModelMap modelMap, @AuthenticationPrincipal UserPrincipal userPrincipal) {

        List<Room> freeRooms = roomService.findFreeRooms(userPrincipal.getUser());
        List<Room> userRooms = roomService.findUserRooms(userPrincipal.getUser());
        List<Game> gamesList = gameService.findAll();

        modelMap.addAttribute("freeRooms", freeRooms);
        modelMap.addAttribute("userRooms", userRooms);
        modelMap.addAttribute("gamesList", gamesList);

        modelMap.addAttribute("createGameDTO", new CreateGameDTO());

        return "rooms";

    }

    @RequestMapping("/room/{id}/show")
    public String room(@PathVariable("id") long id, ModelMap modelMap, @AuthenticationPrincipal UserPrincipal userPrincipal) {

        if (roomService.hasAccess(id, userPrincipal.getUser())) {

            modelMap.addAttribute("roomId", id);
            modelMap.addAttribute("userId", userPrincipal.getUser().getId());

            return "room";
        } else {
            throw new AccessDeniedException("403");
        }
    }

    //TODO zabezpieczyc zeby USER1 nie mogl znow dolaczyc
    @RequestMapping("/room/{id}/join")
    public String roomJoin(@PathVariable("id") long id, @AuthenticationPrincipal UserPrincipal userPrincipal) {

        Room room = roomService.findById(id);

        if (room.getUser2() == null) {

            room.setUser2(userPrincipal.getUser());
            PokemonMemoryCardGameData pokemonMemoryCardGameData = new PokemonMemoryCardGameData();
            room.getGameData().clear();
            room.getGameData().addAll(pokemonMemoryCardGameData.generateDataForDatabase());
            room.setGameStatus(Room.GameStatus.USER1_FIRST_CARD);

            roomService.saveRoom(room);

            return "redirect:/room/" + id + "/show";

        } else {
            throw new AccessDeniedException("403");
        }

    }

    @RequestMapping(value = "/room/add", method = RequestMethod.POST)
    public String roomAdd(@ModelAttribute("createGameDTO") @Valid CreateGameDTO createGameDTO, BindingResult
            resultCreateGameDTO, @AuthenticationPrincipal UserPrincipal userPrincipal) {

        if (!resultCreateGameDTO.hasErrors()) {

            Optional<Game> gameOptional = gameService.findById(createGameDTO.getId());

            Room room = new Room();
            room.setUser1(userPrincipal.getUser());

            if (gameOptional.isPresent()) {
                room.setGame(gameOptional.get());
            } else {
                throw new RuntimeException();
            }

            Room savedRoom = roomService.saveRoom(room);

            if (!room.equals(savedRoom)) throw new RuntimeException();

        }

        return "redirect:/room/all";
    }

    @MessageMapping("/room/{id}/sendChatMessage")
    public void sendChatMessage(@Payload OutgoingMessage outgoingMessage, @DestinationVariable("id") long id) {

        DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("HH:mm:ss | dd-MM-yyyy");

        IncomingMessage incomingMessage = new IncomingMessage();
        incomingMessage.setContent(outgoingMessage.getContent());
        incomingMessage.setUserId(outgoingMessage.getUserId());
        incomingMessage.setDateAndTime(LocalDateTime.now().format(myFormatObj));

        simpMessagingTemplate.convertAndSend("/topic/room/" + id + "/receiveChatMessage", incomingMessage);
    }

}
