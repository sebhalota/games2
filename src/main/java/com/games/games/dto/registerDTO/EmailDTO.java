package com.games.games.dto.registerDTO;

import com.games.games.dto.sequence.First;
import com.games.games.dto.sequence.Second;
import com.games.games.validator.email.CorrectEmail;
import com.games.games.validator.email.NotExistingEmail;

import javax.validation.GroupSequence;
import javax.validation.constraints.Email;

@GroupSequence({EmailDTO.class, First.class, Second.class})
public class EmailDTO {

    @CorrectEmail(groups = First.class)
    @NotExistingEmail(groups = Second.class)
    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
