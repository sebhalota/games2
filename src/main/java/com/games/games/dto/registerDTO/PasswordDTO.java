package com.games.games.dto.registerDTO;

import com.games.games.dto.sequence.First;
import com.games.games.dto.sequence.Second;
import com.games.games.validator.password.PasswordMatches;
import com.games.games.validator.password.StrongEnoughPassword;

import javax.validation.GroupSequence;

@GroupSequence({PasswordDTO.class, First.class, Second.class})
@StrongEnoughPassword(groups = First.class)
@PasswordMatches(groups = Second.class)
public class PasswordDTO {

    private String password;
    private String matchingPassword;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMatchingPassword() {
        return matchingPassword;
    }

    public void setMatchingPassword(String matchingPassword) {
        this.matchingPassword = matchingPassword;
    }
}
