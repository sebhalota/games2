package com.games.games.dto.registerDTO;

import com.games.games.dto.sequence.First;
import com.games.games.dto.sequence.Second;
import com.games.games.dto.sequence.Third;
import com.games.games.validator.username.NotExistingUsername;

import javax.validation.GroupSequence;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@GroupSequence({UsernameDTO.class, First.class, Second.class, Third.class})
public class UsernameDTO {

    @Size(min = 3, max = 10, groups = Second.class)
    @NotExistingUsername(groups = Third.class)
    private String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

}



