package com.games.games.dto.chat;

import java.time.LocalDateTime;
import java.util.Date;

public class IncomingMessage {

    private String content;
    private Long userId;
    private String dateAndTime;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getDateAndTime() {
        return dateAndTime;
    }

    public void setDateAndTime(String dateAndTime) {
        this.dateAndTime = dateAndTime;
    }
}
