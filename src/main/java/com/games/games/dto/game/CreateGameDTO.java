package com.games.games.dto.game;

import com.games.games.validator.game.ExistingGameId;
import com.games.games.validator.game.ExistingGameName;

public class CreateGameDTO {

    @ExistingGameId
    private long id;

    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

}
