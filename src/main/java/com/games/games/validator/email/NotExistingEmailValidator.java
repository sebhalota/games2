package com.games.games.validator.email;

import com.games.games.service.UserService;
import com.games.games.validator.username.NotExistingUsername;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class NotExistingEmailValidator implements ConstraintValidator<NotExistingEmail, String> {


    @Autowired
    private UserService userService;

    public void initialize(NotExistingUsername notExistingUsername) {
    }

    public boolean isValid(String email, ConstraintValidatorContext constraintValidatorContext) {

        if(email != null) {
            return !userService.existsByEmail(email);
        }
        return false;
    }


}
