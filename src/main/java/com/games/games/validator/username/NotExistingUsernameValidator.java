package com.games.games.validator.username;

import com.games.games.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;


import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;


public class NotExistingUsernameValidator implements ConstraintValidator<NotExistingUsername, String> {

    @Autowired
    private UserService userService;

    public void initialize(NotExistingUsername notExistingUsername) {
    }

    public boolean isValid(String username, ConstraintValidatorContext constraintValidatorContext) {

        System.out.println("Zapytanie NotExistingUsernameValidator");

        if(username != null) {
            System.out.println("username nie jest null");

            if(userService == null) {
                System.out.println("Ale userService jest nullem, omg");
            }

            return !userService.existsByUsername(username);
        }
        return false;
    }
}