package com.games.games.validator.username;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.*;

import javax.validation.Constraint;
import javax.validation.Payload;


@Target({FIELD, TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = NotExistingUsernameValidator.class)
@Documented
public @interface NotExistingUsername {

    String message() default "username already exists";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}