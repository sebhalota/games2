package com.games.games.validator.game;


import com.games.games.service.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;


public class ExistingGameIdValidator implements ConstraintValidator<ExistingGameId, Long> {


    @Autowired
    private GameService gameService;

    public void initialize(ExistingGameId existingGameId) {
    }

    public boolean isValid(Long id, ConstraintValidatorContext constraintValidatorContext) {
        return gameService.existsById(id);
    }

}