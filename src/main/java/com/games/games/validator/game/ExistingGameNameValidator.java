package com.games.games.validator.game;


import com.games.games.service.GameService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;


public class ExistingGameNameValidator implements ConstraintValidator<ExistingGameName, String> {


    @Autowired
    private GameService gameService;

    public void initialize(ExistingGameName existingGameName) {
    }

    public boolean isValid(String name, ConstraintValidatorContext constraintValidatorContext) {
        return gameService.existsByName(name);
    }

}