package com.games.games.validator.password;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.games.games.dto.registerDTO.PasswordDTO;

public class PasswordMatchesValidator implements ConstraintValidator<PasswordMatches, PasswordDTO> {

    @Override
    public void initialize(final PasswordMatches constraintAnnotation) {
    }

    @Override
    public boolean isValid(final PasswordDTO passwordDTO, final ConstraintValidatorContext context) {
        if(passwordDTO != null) {
            if(passwordDTO.getPassword() != null && passwordDTO.getMatchingPassword() != null) {
                return passwordDTO.getPassword().equals(passwordDTO.getMatchingPassword());
            }
            return false;
        }
        return false;
    }

}
