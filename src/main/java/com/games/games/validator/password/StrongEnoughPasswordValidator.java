package com.games.games.validator.password;

import com.games.games.dto.registerDTO.PasswordDTO;

import java.util.Arrays;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.passay.*;


public class StrongEnoughPasswordValidator implements ConstraintValidator<StrongEnoughPassword, PasswordDTO> {

    @Override
    public void initialize(final StrongEnoughPassword arg0) {

    }

    @Override
    public boolean isValid(final PasswordDTO passwordDTO, final ConstraintValidatorContext context) {

        final PasswordValidator validator = new PasswordValidator(Arrays.asList(
                new LengthRule(5, 30),
                new WhitespaceRule()));


        if(passwordDTO.getPassword() != null && passwordDTO.getMatchingPassword() != null) {
            final RuleResult result = validator.validate(new PasswordData(passwordDTO.getPassword()));
            if (result.isValid()) {
                return true;
            }

            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(validator.getMessages(result).get(0)).addConstraintViolation();

        }
        return false;
    }

}
