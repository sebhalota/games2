package com.games.games.testingData;

import com.games.games.PokemonMemoryCard.PokemonMemoryCardGameData;
import com.games.games.model.entities.Game;
import com.games.games.model.entities.Room;
import com.games.games.model.entities.User;
import com.games.games.service.GameService;
import com.games.games.service.RoomService;
import com.games.games.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TestingData {

    @Autowired
    UserService userService;

    @Autowired
    GameService gameService;

    @Autowired
    RoomService roomService;

    public void fillDatabaseWithTestData() {

        // USERS
        User stefan = new User("stefan", "stefan@gmail.com", "$2a$11$IXBZ6ccCHLRro5UHeBqSS..n1vrjmmUKJiyVyKxGwfACBD8YZ2Are", true);
        userService.save(stefan);

        User paulina = new User("paulina", "paulina@gmail.com", "$2a$11$IXBZ6ccCHLRro5UHeBqSS..n1vrjmmUKJiyVyKxGwfACBD8YZ2Are", true);
        userService.save(paulina);

        User zosia = new User("zosia", "zosia@gmail.com", "$2a$11$IXBZ6ccCHLRro5UHeBqSS..n1vrjmmUKJiyVyKxGwfACBD8YZ2Are", true);
        userService.save(zosia);

        User faustyn = new User("faustyn", "faustyn@gmail.com", "$2a$11$IXBZ6ccCHLRro5UHeBqSS..n1vrjmmUKJiyVyKxGwfACBD8YZ2Are", true);
        userService.save(faustyn);


        // GAMES
        Game pokemon = new Game("Pokemon Memory Card");
        gameService.save(pokemon);

        Game battleship = new Game("Battleship");
        gameService.save(battleship);

        // ROOM
        Room room1 = new Room();
        room1.setGame(pokemon);
        room1.setGameData(new PokemonMemoryCardGameData().generateDataForDatabase());
        room1.setUser1(zosia);
        room1.setGameStatus(Room.GameStatus.WAITING_FOR_USER2);
        roomService.saveRoom(room1);

    }

}
