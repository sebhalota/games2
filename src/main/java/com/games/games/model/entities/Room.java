package com.games.games.model.entities;

import javax.persistence.*;
import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

// https://stackoverflow.com/questions/30581303/eclipselink-index-on-enumerated-collection

//    @ElementCollection
//    @CollectionTable(name = "room_game_data", joinColumns = @JoinColumn(name = "id"), indexes={@Index(columnList="game_data")})
//    @Column(name = "game_data")

//    @CollectionTable(name = "room_game_data", indexes={@Index(columnList="gameData")})
// @ElementCollection(targetClass = Long.class)

//    @ElementCollection
//    @CollectionTable(name = "room_game_data")
//    @OrderColumn(name ="index")
//    private List<Long> gameData = new ArrayList<>();


@Entity
@Transactional
public class Room {

    public enum GameStatus {
        WAITING_FOR_USER2,
        USER1_FIRST_CARD,
        USER1_SECOND_CARD,
        USER2_FIRST_CARD,
        USER2_SECOND_CARD,
        USER1_WON,
        USER2_WON,
        DRAW
    }

    @Id
    @NotNull
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @NotNull
    @ManyToOne
    @JoinColumn
    private Game game;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "room_game_data")
    @OrderColumn
    private List<Long> gameData = new ArrayList<>();

    @NotNull
    @ManyToOne
    @JoinColumn
    private User user1;

    @ManyToOne
    @JoinColumn
    private User user2;

    @NotNull
    private Room.GameStatus gameStatus;

    public Room() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Game getGame() {
        return game;
    }

    public List<Long> getGameData() {
        return gameData;
    }

    public void setGameData(List<Long> gameData) {
        this.gameData = gameData;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public User getUser1() {
        return user1;
    }

    public void setUser1(User user1) {
        this.user1 = user1;
    }

    public User getUser2() {
        return user2;
    }

    public void setUser2(User user2) {
        this.user2 = user2;
    }

    public GameStatus getGameStatus() {
        return gameStatus;
    }

    public void setGameStatus(GameStatus gameStatus) {
        this.gameStatus = gameStatus;
    }

}
