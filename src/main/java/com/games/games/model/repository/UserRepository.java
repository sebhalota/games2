package com.games.games.model.repository;

import com.games.games.model.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;

public interface UserRepository extends JpaRepository<User, Long> {

    User findByUsername(String username);

    boolean existsByUsername(String username);
    boolean existsByEmail(String email);

    User findById(long id);

//    @Transactional
//    @Modifying(clearAutomatically = true)
//    @Query("UPDATE User u SET u.username = :username WHERE u.id = :id")
//    int updateUsername(@Param("id") long id, @Param("username") String username);

}
