package com.games.games.model.repository;

import com.games.games.model.entities.Game;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;

public interface GameRepository extends JpaRepository<Game, Long> {

    boolean existsByName(String name);

//    @Transactional
//    @Modifying(clearAutomatically = true)
//    @Query("UPDATE Game g SET g.data1 = :data1 WHERE g.id = :id")
//    int updateData1(@Param("id") long id, @Param("data1") long data1);

}
