package com.games.games.model.repository;


import com.games.games.model.entities.Room;
import com.games.games.model.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface RoomRepository extends JpaRepository<Room, Long> {

    //TODO dowiedziec sie co to jest FETCH
//    @Query("SELECT r FROM Room r JOIN FETCH r.gameData WHERE r.id = (:id)")
    Room findById(long id);

    Room findByIdAndUser1(long id, User user);
    Room findByIdAndUser2(long id, User user);

    List<Room> findByUser1NotAndUser2(@Param("user1") User user1, @Param("user2") User user2);
    List<Room> findByUser1OrUser2(@Param("user1") User user1, @Param("user2") User user2);

    @Query("FROM Room r WHERE r.id = :id AND (r.user1 = :user1 OR r.user2 = :user2)")
    Room findByIdAndUser1orUser2(@Param("id") long id, @Param("user1") User user1, @Param("user2") User user2);

//    @Transactional
//    @Modifying(clearAutomatically = true)
//    @Query("UPDATE Room r SET r.user2 = :user2 WHERE r.id = :id")
//    int updateUser2(@Param("id") long id, @Param("user2") User user2);

//    @Transactional
//    @Modifying(clearAutomatically = true)
//    @Query("UPDATE Room r SET r.userMovement = :userMovement WHERE r.id = :id")
//    int updateUserMovement(@Param("id") long id, @Param("userMovement") GameStatus userMovement);

    // NIE DZIALA
//    @Transactional
//    @Modifying(clearAutomatically = true)
//    @Query("UPDATE Room r SET r.gameData = :gameData WHERE r.id = :id")
//    int updateGameData(@Param("id") long id, @Param("gameData") List<Long> gameData);

//
//    @Transactional
//    @Modifying(clearAutomatically = true)
//    @Query("UPDATE Room r SET r.room_game_data.game_data = :gameData WHERE r.room_game_data.room_id = :id AND r.room_game_data.index = :index")
//    int updateGameData(@Param("id") long id, @Param("index") int index, @Param("gameData") long gameData);

//    @Transactional
//    @Modifying(clearAutomatically = true)
//    @Query("UPDATE Room r SET r.gameData = :gameData WHERE r.id in (:id)")
//    int updateGameData(@Param("id") long id, @Param("gameData") List<Long> gameData);


//    @Transactional
//    @Modifying(clearAutomatically = true)
//    @Query("UPDATE Room_Game_Data r SET r.game_data = :game_data WHERE r.room_id = :room_id AND r.index = :index")
//    int updateGameData(@Param("room_id") long room_id, @Param("index") long index, @Param("game_data") long game_data);


//    boolean existsById(long id);
//    List<Room> findByUser2IsNull();
//    List<Room> findByGame_Id(long gameId);
//    List<Room> findByGame_IdAndUser2(long gameId, User user);

}
