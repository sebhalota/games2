package com.games.games.service;

import com.games.games.model.entities.Game;
import com.games.games.model.repository.GameRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class GameService {

    @Autowired
    GameRepository gameRepository;

    public List<Game> findAll() {
        return gameRepository.findAll();
    }

    public Optional<Game> findById(Long id) {
        return gameRepository.findById(id);
    }

    public boolean existsById(Long id) {
        return gameRepository.existsById(id);
    }

    public boolean existsByName(String name) {
        return gameRepository.existsByName(name);
    }

    public Game save(Game game) {
        return gameRepository.save(game);
    }

    public List<Game> saveAll(List<Game> games) {
        return gameRepository.saveAll(games);
    }

}
