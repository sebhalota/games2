package com.games.games.service;

import com.games.games.model.entities.Room;
import com.games.games.model.entities.User;
import com.games.games.model.repository.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoomService {

    @Autowired
    RoomRepository roomRepository;

    public Room findById(long id) {
        return roomRepository.findById(id);
    }

    public Room saveRoom(Room room) {
        return roomRepository.save(room);
    }

    public List<Room> findUserRooms(User user) {
        return roomRepository.findByUser1OrUser2(user,user);
    }

    public List<Room> findFreeRooms(User user) {
        return roomRepository.findByUser1NotAndUser2(user,null);
    }

//    public void join(long roomId, User user) {
//        roomRepository.updateUser2(roomId, user);
//    }

//    public int updateUserMovement(long id, Room.GameStatus userMovement) {
//        return roomRepository.updateUserMovement(id, userMovement);
//    }
//
//    public int updateGameData(long id, List<Long> gameData) {
//        return roomRepository.updateGameData(id,gameData);
//    }

    public boolean isFree(long id) {
        if (roomRepository.findByIdAndUser2(id, null) != null) return true;
        else return false;
    }

    public boolean hasAccess(long id, User user) {
        if (roomRepository.findByIdAndUser1orUser2(id, user, user) != null) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isOwner(long id, User user) {
        return roomRepository.findByIdAndUser1(id,user) != null;
    }

    public boolean isGuest(long id, User user) {
        return roomRepository.findByIdAndUser2(id,user) != null;
    }

//    public boolean isOwner(long id, User user) {
//
//        return false;
//    }

    //    public List<Room> findAll() {
//        return roomRepository.findAll();
//    }

//    public List<Room> findByUser2IsNull() {
//        return roomRepository.findByUser2IsNull();
//    }

//    public List<Room> findByGame_Id(long game_id) {
//        return roomRepository.findByGame_Id(game_id);
//    }

//    public List<Room> findFreeRooms() {
//        return roomRepository.findByUser2IsNull();
//    }

//    public List<Room> findFreeRooms(long gameId) {
//        return roomRepository.findByGame_IdAndUser2(gameId, null);
//    }

    //    public boolean existsById(long id) {
//        return roomRepository.existsById(id);
//    }


}
