package com.games.games.service;

import com.games.games.model.entities.User;
import com.games.games.model.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public User findById(long id) {
        return userRepository.findById(id);
    }

    public boolean existsByUsername(String username) {
        if (username != null) {
            return userRepository.existsByUsername(username);
        }
        return false;
    }

    public boolean existsByEmail(String email) {

        if (email != null) {
            return userRepository.existsByEmail(email);
        }
        return false;
    }

    public User save(User s) {
        return userRepository.save(s);
    }

    public List<User> saveAll(List<User> users) {
        return userRepository.saveAll(users);
    }

//    public List<User> findAll() {
//        return userRepository.findAll();
//    }
//

//
//    public User findByUsername(String username) {
//        return userRepository.findByUsername(username);
//    }

//    public void updateUsername(long id, String username) {
//        userRepository.updateUsername(id,username);
//    }

}
