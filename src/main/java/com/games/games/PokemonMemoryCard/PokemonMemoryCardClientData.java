package com.games.games.PokemonMemoryCard;

import com.games.games.model.entities.Room;

import java.util.ArrayList;
import java.util.List;

public class PokemonMemoryCardClientData {

    private Room.GameStatus gameStatus;
    private String username1;
    private String username2;
    private long user1Score;
    private long user2Score;
    private List<CardStatus> cardStatusList = new ArrayList<>();

    private int timeout1;
    private int timeout2;
    private boolean updateTimeout;

    public PokemonMemoryCardClientData(PokemonMemoryCardGameData data, Room room) {
        this.gameStatus = room.getGameStatus();

        if (room.getUser1() != null) {
            this.username1 = room.getUser1().getUsername();
        } else {
            this.username1 = "...";
        }

        if (room.getUser2() != null) {
            this.username2 = room.getUser2().getUsername();
        } else {
            this.username2 = "...";
        }

        this.user1Score = data.getUser1Score();
        this.user2Score = data.getUser2Score();
        cardStatusList.addAll(data.getCardStatusList());
    }

    public Room.GameStatus getGameStatus() {
        return gameStatus;
    }

    public void setGameStatus(Room.GameStatus gameStatus) {
        this.gameStatus = gameStatus;
    }

    public String getUsername1() {
        return username1;
    }

    public void setUsername1(String username1) {
        this.username1 = username1;
    }

    public String getUsername2() {
        return username2;
    }

    public void setUsername2(String username2) {
        this.username2 = username2;
    }

    public int getTimeout1() {
        return timeout1;
    }

    public void setTimeout1(int timeout1) {
        this.timeout1 = timeout1;
    }

    public int getTimeout2() {
        return timeout2;
    }

    public void setTimeout2(int timeout2) {
        this.timeout2 = timeout2;
    }

    public long getUser1Score() {
        return user1Score;
    }

    public void setUser1Score(long user1Score) {
        this.user1Score = user1Score;
    }

    public long getUser2Score() {
        return user2Score;
    }

    public void setUser2Score(long user2Score) {
        this.user2Score = user2Score;
    }

    public List<CardStatus> getCardStatusList() {
        return cardStatusList;
    }

    public void setCardStatusList(List<CardStatus> cardStatusList) {
        this.cardStatusList = cardStatusList;
    }

    public boolean isUpdateTimeout() {
        return updateTimeout;
    }

    public void setUpdateTimeout(boolean updateTimeout) {
        this.updateTimeout = updateTimeout;
    }
}
