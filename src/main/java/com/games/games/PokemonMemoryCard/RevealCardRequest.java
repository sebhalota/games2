package com.games.games.PokemonMemoryCard;

public class RevealCardRequest {

    private Long userId;
    private int cardNr;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public int getCardNr() {
        return cardNr;
    }

    public void setCardNr(int cardNr) {
        this.cardNr = cardNr;
    }
}
