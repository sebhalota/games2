package com.games.games.PokemonMemoryCard;

public class CardStatus {

    private int imgNr;
    private CardStatusEnum cardStatusEnum = CardStatusEnum.COVERED;

    public int getImgNr() {
        return imgNr;
    }

    public void setImgNr(int imgNr) {
        this.imgNr = imgNr;
    }

    public CardStatusEnum getCardStatusEnum() {
        return cardStatusEnum;
    }

    public void setCardStatusEnum(CardStatusEnum cardStatusEnum) {
        this.cardStatusEnum = cardStatusEnum;
    }

    public enum CardStatusEnum {
        COVERED,
        REMOVED,
        REVEAL,
        REVEAL_AND_REMOVE,
        REVEAL_AND_COVER
    }

    @Override
    public String toString() {
        return "imgNr: " + imgNr + ", cardStatusEnum: " + cardStatusEnum;
    }

}
