package com.games.games.PokemonMemoryCard;


import com.games.games.model.entities.Room.GameStatus;

import java.util.ArrayList;
import java.util.List;

import static com.games.games.PokemonMemoryCard.CardStatus.CardStatusEnum.COVERED;
import static com.games.games.PokemonMemoryCard.CardStatus.CardStatusEnum.REMOVED;
import static com.games.games.PokemonMemoryCard.CardStatus.CardStatusEnum.REVEAL_AND_REMOVE;
import static com.games.games.PokemonMemoryCard.CardStatus.CardStatusEnum.REVEAL_AND_COVER;

public class PokemonMemoryCardGameData {

    private static final int DECK_SIZE = 12;
    private long user1Score;
    private long user2Score;
    private List<CardStatus> cardStatusList = new ArrayList<>();

    public PokemonMemoryCardGameData() {
        List<Integer> imgNr = drawDeck();
        for (int i = 0; i < DECK_SIZE; i++) {
            CardStatus c = new CardStatus();
            c.setImgNr(imgNr.get(i));
            c.setCardStatusEnum(CardStatus.CardStatusEnum.COVERED);
            cardStatusList.add(c);
        }
    }

    //TODO zabezpieczyc
    public PokemonMemoryCardGameData(List<Long> data) {
        List<Integer> imgNr2 = data1ToImgNr(data.get(0));
        List<Boolean> covered2 = data2ToCovered(data.get(1));
        List<Boolean> unremoved2 = data3ToUnremoved(data.get(2));
        long user1Score2 = data.get(3);
        long user2Score2 = data.get(4);

        for (int i = 0; i < DECK_SIZE; i++) {
            CardStatus c = new CardStatus();
            c.setImgNr(imgNr2.get(i));

            if (!unremoved2.get(i)) c.setCardStatusEnum(REMOVED);
            else if (!covered2.get(i)) c.setCardStatusEnum(CardStatus.CardStatusEnum.REVEAL);
            else c.setCardStatusEnum(CardStatus.CardStatusEnum.COVERED);

            cardStatusList.add(c);

            user1Score = user1Score2;
            user2Score = user2Score2;
        }
    }

    public static int getDeckSize() {
        return DECK_SIZE;
    }

    public long getUser1Score() {
        return user1Score;
    }

    public void setUser1Score(long user1Score) {
        this.user1Score = user1Score;
    }

    public long getUser2Score() {
        return user2Score;
    }

    public void setUser2Score(long user2Score) {
        this.user2Score = user2Score;
    }

    public List<CardStatus> getCardStatusList() {
        return cardStatusList;
    }

    public void setCardStatusList(List<CardStatus> cardStatusList) {
        this.cardStatusList = cardStatusList;
    }

    private static List<Integer> data1ToImgNr(long data1) {
        List<Integer> list = new ArrayList<>();

        for (int i = 0; i < DECK_SIZE; i++) {
            Integer x = (int) (((0xFL << i * 4) & (data1)) >> i * 4);
            list.add(x);
        }

        return list;
    }

    private static long ImgNrToData1(List<Integer> list) {
        long data1 = 0;
        for (int i = 0; i < list.size(); i++) {
            data1 |= (long) (list.get(i)) << (long) (i * 4);
        }
        return data1;
    }

    private static List<Boolean> data2ToCovered(long data2) {
        List<Boolean> list = new ArrayList<>();

        for (int i = 0; i < DECK_SIZE; i++) {
            Boolean b = (data2 & (1 << i)) > 0;
            list.add(b);
        }

        return list;
    }

    private static long coveredToData2(List<Boolean> list) {
        long data2 = 0;

        for (int i = 0; i < list.size(); i++) {
            if (list.get(i)) {
                data2 |= (1 << i);
            }
        }

        return data2;
    }

    private static List<Boolean> data3ToUnremoved(long data3) {
        List<Boolean> list = new ArrayList<>();

        for (int i = 0; i < DECK_SIZE; i++) {
            Boolean b = (data3 & (1 << i)) > 0;
            list.add(b);
        }

        return list;
    }

    private static long unremovedToData3(List<Boolean> list) {
        long data3 = 0;

        for (int i = 0; i < list.size(); i++) {
            if (list.get(i)) {
                data3 |= (1 << i);
            }
        }

        return data3;
    }

    //TODO zrobic losowe
    private static List<Integer> drawDeck() {
        List<Integer> list = new ArrayList<>();

        for (int i = 0; i < DECK_SIZE / 2; i++) {
            Integer x = i + 1;
            list.add(x);
        }
        for (int i = 0; i < DECK_SIZE / 2; i++) {
            Integer x = i + 1;
            list.add(x);
        }

        return list;
    }

    public List<Long> generateDataForDatabase() {
        List<Long> list = new ArrayList<>();

        List<Integer> imgNr = new ArrayList<>();
        List<Boolean> covered = new ArrayList<>();
        List<Boolean> unremoved = new ArrayList<>();

        for (CardStatus c : cardStatusList) {
            imgNr.add(c.getImgNr());
            unremoved.add(!c.getCardStatusEnum().equals(REMOVED) && !c.getCardStatusEnum().equals(REVEAL_AND_REMOVE));
            covered.add(c.getCardStatusEnum().equals(COVERED) || c.getCardStatusEnum().equals(REVEAL_AND_COVER));
        }

        list.add(ImgNrToData1(imgNr));
        list.add(coveredToData2(covered));
        list.add(unremovedToData3(unremoved));
        list.add(user1Score);
        list.add(user2Score);

        return list;
    }

    // TODO warning
    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();

        str.append("user1Score: " + user1Score + ", user2Score: " + user2Score + "\n\r");
        for (CardStatus c : cardStatusList) {
            str.append(c.toString() + "\n\r");
        }

        return str.toString();
    }

}
