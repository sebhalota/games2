var stompClient = null;

function sendMessage() {

    if($("#textBosMsg").val() !== "") {
            stompClient.send('/app/room/'+ roomId + '/sendChatMessage', {}, JSON.stringify({
                'content': $("#textBosMsg").val(),
                'userId' : userId
            }));
    }

    $("#textBosMsg").val("");

}

function getIncomingMsgTemplate(date, msg) {
    return  '<div class="incoming_msg"> <div class="received_msg"> <div class="received_withd_msg">' +
                '<p>' + msg +'</p>' +
                '<span class="time_date">' + date + '</span>' +
            '</div> </div> </div>';
}

function getOutgoingMsgTemplate(date, msg) {
    return '<div class="outgoing_msg"> <div class="sent_msg">' +
                '<p>'+ msg + '</p>' +
                '<span class="time_date">' + date + '</span>' +
           '</div> </div>';
}

function connect() {
    var socket = new SockJS('/roomChat');
    stompClient = Stomp.over(socket);

    stompClient.connect({}, function (frame) {
        stompClient.subscribe('/topic/room/' + roomId + '/receiveChatMessage', function (msg) {
              showMessage(msg);
        });
    });

}

function showMessage(msg) {

    var json = JSON.parse(msg.body);

    if(userId == json.userId) {
        $("#msg_history").append(getOutgoingMsgTemplate(json.dateAndTime,json.content));
    }
    else {
        $("#msg_history").append(getIncomingMsgTemplate(json.dateAndTime,json.content));
    }

}

$(function () {
    $( "#send" ).click(function() { sendMessage(); });
    connect();
});