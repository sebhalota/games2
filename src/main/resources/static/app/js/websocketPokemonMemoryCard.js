var PMC_stompClient = null;

function PMC_connect() {
    var socket = new SockJS('/pokemonMemoryCard');
    PMC_stompClient = Stomp.over(socket);

    PMC_stompClient.connect({}, function (frame) {
        PMC_stompClient.subscribe('/topic/pokemonMemoryCard/' + roomId + '/gameData', function (pokemonMemoryCardClientData) {
            PMC_handleGameStatus(pokemonMemoryCardClientData)
        });
    });

}

function PMC_handleGameStatus(pokemonMemoryCardClientData) {

    var json = JSON.parse(pokemonMemoryCardClientData.body);

    for (var i = 0; i < json.cardStatusList.length; i++) {
        switch((json.cardStatusList[i].cardStatusEnum)) {
            case "COVERED":
                PMC_coverCard(i);
            break;

            case "REMOVED":
                PMC_removeCard(i);
            break;

            case "REVEAL":
                PMC_revealCard(i,json.cardStatusList[i].imgNr);
            break;

            case "REVEAL_AND_REMOVE":
                PMC_revealAndRemoveCard(i,json.cardStatusList[i].imgNr);
            break;

            case "REVEAL_AND_COVER":
                PMC_revealAndCoverCard(i,json.cardStatusList[i].imgNr);
            break;
        }
    }

    $('#user1Score').html(json.user1Score);
    $('#user2Score').html(json.user2Score);
    $('#username1').html(json.username1);
    $('#username2').html(json.username2);
    $('#gameStatus').html(json.gameStatus);

    if(json.updateTimeout == true) {
        $('#timeout1').html(json.timeout1);
        $('#timeout2').html(json.timeout2);
    }

}

function PMC_removeCard(cardNr) {
    $('#c'+cardNr).css('opacity', '0');
//    $('#c'+cardNr).html('<img src="/app/img/pokemonMemoryCard/card.png">');
    $('#c'+cardNr).css('background-image', 'url(/app/img/pokemonMemoryCard/card.png)');
}

function PMC_coverCard(cardNr) {
    $('#c'+cardNr).css('opacity', '100');
//    $('#c'+cardNr).html('<img src="/app/img/pokemonMemoryCard/card.png">');
    $('#c'+cardNr).css('background-image', 'url(/app/img/pokemonMemoryCard/card.png)');
}

function PMC_revealCard(cardNr, imgNr) {
    $('#c'+cardNr).css('opacity', '100');
//    $('#c'+cardNr).html('<img src="/app/img/pokemonMemoryCard/' + imgNr + '.png">');
    $('#c'+cardNr).css('background-image', 'url(/app/img/pokemonMemoryCard/' + imgNr + '.png)');
}

function PMC_revealAndRemoveCard(cardNr, imgNr) {
    PMC_revealCard(cardNr, imgNr);
    setTimeout(function() { PMC_removeCard(cardNr) }, 1000);
}

function PMC_revealAndCoverCard(cardNr, imgNr) {
    PMC_revealCard(cardNr, imgNr);
    setTimeout(function() { PMC_coverCard(cardNr) }, 1000);
}

function PMC_revealCardRequest(cardNr) {

   PMC_stompClient.send('/app/pokemonMemoryCard/'+ roomId + '/reveal', {}, JSON.stringify({
        'userId' : userId,
        'cardNr': cardNr
    }));
}

function PMC_refreshRequest() {

   PMC_stompClient.send('/app/pokemonMemoryCard/'+ roomId + '/refresh', {}, null);
}


function PMC_timeoutCountdown() {

    PMC_decreaseTime(1);
    PMC_decreaseTime(2);

    setTimeout(function() { PMC_timeoutCountdown() }, 1000);
}

function PMC_decreaseTime(nr) {
    var x =  parseInt($('#timeout' + nr).html());
    if(x > 0) x--;
    $('#timeout' + nr).html(x);
    x = x * 10;
    $('#timeoutBar'+nr).css('width', x + '%');

}


$(function () {

    $( ".pokemonCard" ).click(function() {
        PMC_revealCardRequest(this.id.substring(1));
    });

    PMC_connect();

    setTimeout(function() { PMC_refreshRequest() }, 100);

    PMC_timeoutCountdown();

});
